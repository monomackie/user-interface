const { ipcMain } = require('electron')
const net = require('net');
const client = new net.Socket();

client.on('connect', function() {
    eventConnect();
})
client.on('error', function(e) { //don't let error beeing show in an ugly windows dialog box
    //console.error("Error catched : ", e)
});

let _webContents;

let _connecting = false;
let _connectionState = false;

const CHECK_TIME_MS = 3000;

function reinitCallbacks() {
    client.removeAllListeners('data');
    client.removeAllListeners('close');

    //ipcMain.removeAllListeners('eventFromTheFrontChannel'); //TODO This how to receive events from the front
}

function processTCPMessage(dataJson) {
    if(dataJson.hasOwnProperty('isMMConnected')){
        _webContents.send('statusUpdate', {isMMConnected: dataJson.isMMConnected});
    }
}

function setupCallbacks(){
    client.on('data', function(data) {
        try {
            let dataJson = JSON.parse(data);
            console.info('Received from the driver: ', dataJson);
            processTCPMessage(dataJson);
        } catch (e) {
            console.error('Error while processing message from server : ', data);
            console.error(e)
        }
    });

    client.on('close', function() {
        eventDisconnect()
    });

    /*
        ipcMain.on('eventFromTheFrontChannel', (event, arg) => { //TODO This how to receive events from the front
            console.log("DBG : Message from the front", arg)
        })
    */
}

function eventConnect(){
    console.log('Connected to status server');
    _connectionState = true;
    setupCallbacks()
    _webContents.send('statusUpdate',
        {driverConnection: "OK"}
    );

    let request = {ask: 'StateMM'}; //request initial test to statusServer
    client.write(JSON.stringify(request) + "\0");
}

function eventDisconnect(){
    console.log('Disconnected from status server');
    _connectionState = false;
    reinitCallbacks();
    _webContents.send('statusUpdate',
        {
            driverConnection: "KO",
            isMMConnected: "Inconnu"
        }
    );
}

function connect() {
    client.connect(48651, '127.0.0.1');
}

function connectIfNotConnecting(){
    if(!_connecting && !_connectionState) {
        _connecting = true;
        connect();
        _connecting = false;
    }
}

module.exports = {
    launch: function (webContents){
        _webContents = webContents;

        connectIfNotConnecting();

        setInterval( //retry periodically
            () => {
                connectIfNotConnecting();
            },
            CHECK_TIME_MS
        );
    }
};